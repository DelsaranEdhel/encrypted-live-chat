# Encrypted Live Chat

A simple encrypted local live chat written in C# .NET and WPF.

When user has joined to the server, his public RSA key is being broadcasted to the other users. After public RSA keys have been exchanged, the public Diffie-Hellman keys are being exchanged.
Message sent by the user is being encrypted with the use of AES and RSA. Also the given message is being ecrypted by using the hash function SHA256. After that the secret Diffie-Hellman key is being sent to identify the user. When the other user receives the message, it is being decrypted by using the AES and RSA. After that the given message is being compared with the given hash. If hashses are the same then the message was not modified and it is being displayed on the chat.

![screen-1](screen-1.png)