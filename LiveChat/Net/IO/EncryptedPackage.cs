﻿using System.Runtime.InteropServices;
using System;

namespace ChatClient.Net.IO
{
    /// <summary>
    /// Convert a struct to byte and conversly
    /// https://stackoverflow.com/questions/3278827/how-to-convert-a-structure-to-a-byte-array-in-c
    /// </summary>

    struct EncryptedPackage
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 500)]
        public string encryptedMessage;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 500)]
        public string encryptedAESIV;

        public EncryptedPackage(string encryptedMessage, string encryptedAESIV)
        {
            this.encryptedMessage = encryptedMessage;
            this.encryptedAESIV = encryptedAESIV;
        }

        /// <summary>
        /// Convert EncryptedPackage to bytes
        /// </summary>
        /// <param name="package">Package to convert to bytes</param>
        /// <returns></returns>
        public static byte[] GetBytes(EncryptedPackage package)
        {
            int size = Marshal.SizeOf(package);
            byte[] arr = new byte[size];

            IntPtr ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(package, ptr, true);
                Marshal.Copy(ptr, arr, 0, size);
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return arr;
        }

        /// <summary>
        /// Convert byte[] to EncryptedPackage
        /// </summary>
        /// <param name="arr">Byte array to convert to EncryptedPackage</param>
        /// <returns></returns>
        public static EncryptedPackage FromBytes(byte[] arr)
        {
            EncryptedPackage str = new EncryptedPackage();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);

                Marshal.Copy(arr, 0, ptr, size);

                str = (EncryptedPackage)Marshal.PtrToStructure(ptr, str.GetType());
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return str;
        }
    }
}
