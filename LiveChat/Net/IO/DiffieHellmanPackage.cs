﻿using System;
using System.Security.Cryptography;
using System.Windows;

namespace ChatClient.Net.IO
{
    /// <summary>
    /// Stores the DiffieHellman key for this user, e.g. A
    /// Stores a GUID of a user from which we received a public key, e.g. B (this key will be used for sending to him messages)
    /// Stores a public diffie-hellman of user B
    /// </summary>

    public class DiffieHellmanPackage
    {
        public Guid uid;
        public ECDiffieHellmanCng diffieHellmanCng;
        public byte[] friendPublicKey;

        public DiffieHellmanPackage(Guid uid)
        {
            this.uid = uid;

            diffieHellmanCng = new ECDiffieHellmanCng(ECCurve.NamedCurves.nistP256);
            diffieHellmanCng.KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash;
            diffieHellmanCng.HashAlgorithm = CngAlgorithm.Sha256;
        }

        public byte[] GetPublicKey() =>
            diffieHellmanCng.PublicKey.ToByteArray();

        /// <summary>
        /// Create key material - our secret key
        /// </summary>
        /// <returns></returns>
        public byte[] GetFinalKey()
        {
            //MessageBox.Show("Using friend key - " + friendPublicKey.Length);
            if (friendPublicKey == null)
            {
                MessageBox.Show("Friend public key is null!");
                return null;
            }

            CngKey otherUserKey = CngKey.Import(friendPublicKey, CngKeyBlobFormat.EccPublicBlob);
            return diffieHellmanCng.DeriveKeyMaterial(otherUserKey);
        }

        public void SaveFriendPublicKey(byte[] publicKey)
        {
            friendPublicKey = publicKey;
            //MessageBox.Show("Friend public key was set and his length is " + friendPublicKey.Length);
        }
    }
}
