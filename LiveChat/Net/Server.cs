﻿using ChatClient.Crypto;
using ChatClient.Net.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;

namespace ChatClient.NET
{
    /// <summary>
    /// Server on the client side
    /// </summary>
    class Server
    {
        private TcpClient _client;
        public PacketReader packetReader;

        public event Action connectedEvent;
        public event Action msgReceivedEvent;
        public event Action<string> msgReceivedEventTest;
        public event Action userDisconnectEvent;

        private readonly CspParameters _cspp = new CspParameters();
        private RSACryptoServiceProvider _rsa;

        public string PublicKeyRSA { get; set; }

        /// <summary>
        /// Stores a publicRSAKey and the GUID of the user to which this key belong
        /// </summary>
        public struct RsaPackage
        {
            public Guid ownerUID;
            public string publicRSAKey;

            public RsaPackage(Guid uid, string key)
            {
                this.ownerUID = uid;
                this.publicRSAKey = key;
            }
        }

        public static List<RsaPackage> rsaPackages = new List<RsaPackage>();
        public static List<DiffieHellmanPackage> diffieHellmanPackages = new List<DiffieHellmanPackage>();

        public Server()
        {
            _client = new TcpClient();

            //when user opens an app, he will create his RSA keys
            CreateRSAKeys();
        }

        private void CreateRSAKeys()
        {
            //Create an RSA keys pair
            _cspp.KeyContainerName = GenerateKeyName(10); //key name
            _rsa = new RSACryptoServiceProvider(_cspp)
            {
                PersistKeyInCsp = true
            };
            PublicKeyRSA = _rsa.ToXmlString(false);
        }

        private string GenerateKeyName(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public void ConnectToServer(string username)
        {
            if (!_client.Connected)
            {
                _client.Connect("127.0.0.1", 7891);
                packetReader = new PacketReader(_client.GetStream());

                if(!string.IsNullOrEmpty(username))
                {
                    var connectPacket = new PacketBuilder();
                    connectPacket.WriteOpCode(0);
                    connectPacket.WriteMessage(username);
                    _client.Client.Send(connectPacket.GetPacketBytes());
                }

                //after user has connected, from now on we will be always listening for packets
                ReadPackets();
            }

            //when user has connected to the server, then broadcast his public RSA key
            SendRSAPublicKeyToServer();
        }

        private void ReadPackets()
        {
            Task.Run(() =>
            {
                while (true)
                {   
                    var opcode = packetReader.ReadByte();
                    /*
                     * Opcodes:
                     * 1 - execute event which will show a message about what user has connected to the chat
                     * 3 - save RSA public key and uid of the user to which the key belong
                     * 4 - save Diffie-Hellman public key and uid of the user to which the key belong AND send back to this user the Diffie-Hellman key of THIS user
                     * 5 - decrypt a message and display it
                     * 6 - save Diffie-Hellman public key and uid of the user to which the key belong (without sending it back, because keys were already exchanged)
                     * 10 - execute event which will show a message about what user has disconnected from the chat
                     */
                    switch (opcode)
                    {
                        case 1:
                            connectedEvent?.Invoke();
                            break;
                        case 3:
                            string uid = packetReader.ReadMessage();
                            string key = packetReader.ReadPublicRSAKey();
                            rsaPackages.Add(new RsaPackage(Guid.Parse(uid), key));

                            DiffieHellmanPackage diffieHellmanPackage = new DiffieHellmanPackage(Guid.Parse(uid));
                            diffieHellmanPackages.Add(diffieHellmanPackage);
                            var messagePacket = new PacketBuilder();
                            messagePacket.WriteOpCode(4);
                            messagePacket.WriteUID(Guid.Parse(uid));
                            messagePacket.WriteDiffieHellmanKey(diffieHellmanPackage.GetPublicKey());
                            _client.Client.Send(messagePacket.GetPacketBytes());

                            MessageBox.Show("Public RSA key received on Client side - " + uid);
                            break;
                        case 4:
                            Guid fromUid = packetReader.ReadUID();
                            byte[] diffieHellmanKey = packetReader.ReadDiffieHellmanKey();

                            foreach(DiffieHellmanPackage package in diffieHellmanPackages)
                            {
                                if(package.uid == fromUid)
                                {
                                    package.SaveFriendPublicKey(diffieHellmanKey);

                                    var messagePackett = new PacketBuilder();
                                    messagePackett.WriteOpCode(6);
                                    messagePackett.WriteUID(fromUid);
                                    messagePackett.WriteDiffieHellmanKey(package.GetPublicKey());
                                    _client.Client.Send(messagePackett.GetPacketBytes());

                                    break;
                                }
                            }
                            break;
                        case 5:
                            Guid thisUid = packetReader.ReadUID();
                            string username = packetReader.ReadMessage();
                            EncryptedPackage encryptedPackage = packetReader.ReadEncryptedPackage();

                            byte[] diffieHellmanKeyToDecrypt = null;
                            foreach (DiffieHellmanPackage package in diffieHellmanPackages)
                            {
                                if (package.uid == thisUid)
                                {
                                    diffieHellmanKeyToDecrypt = package.GetFinalKey();
                                    break;
                                }
                            }
                            if (diffieHellmanKeyToDecrypt == null)
                                MessageBox.Show("Diffie-Hellman key is null!");

                            MessageBox.Show("Decrypting..." +
                                "\n\nEncrypted Message: " + encryptedPackage.encryptedMessage +
                                "\n\nDiffie-Hellman length with key material: " + diffieHellmanKeyToDecrypt.Length +
                                "\n\nRSA public key: " + _rsa.ToXmlString(false)
                                );

                            string msg = CryptoClass.Decrypt(encryptedPackage, diffieHellmanKeyToDecrypt, _rsa);
                            msgReceivedEventTest?.Invoke($"[{DateTime.Now}] {username}: " + msg);
                            break;
                        case 6:
                            MessageBox.Show("Diffie-Hellman keys were exchanged!");
                            Guid fromUid2 = packetReader.ReadUID();
                            byte[] publicDiffieHellmanKey = packetReader.ReadDiffieHellmanKey();

                            foreach (DiffieHellmanPackage package in diffieHellmanPackages)
                            {
                                if (package.uid == fromUid2)
                                {
                                    package.SaveFriendPublicKey(publicDiffieHellmanKey);
                                    break;
                                }
                            }
                            break;
                        case 10:
                            userDisconnectEvent?.Invoke();
                            break;
                        default:
                            Console.WriteLine("Unsupported opcode = " + opcode);
                            break;
                    }
                }
            });
        }

        public void SendRSAPublicKeyToServer()
        {
            var messagePacket = new PacketBuilder();
            messagePacket.WriteOpCode(3);
            messagePacket.WritePublicRSAKey(PublicKeyRSA);
            _client.Client.Send(messagePacket.GetPacketBytes());
        }

        public void SendMessageToServer(string message)
        {
            foreach (RsaPackage rsaPackage in rsaPackages)
            {
                //1. Set opcode to 5 - sending a message
                var messagePacket = new PacketBuilder();
                messagePacket.WriteOpCode(5);

                //2. Get rsa public key for target user (rsaPackage.ownerUID)
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(rsaPackage.publicRSAKey);

                //3. Find and get the diffie-hellman key material with target user (rsaPackage.ownerID)
                byte[] diffieHellmanKey = null;
                foreach(DiffieHellmanPackage diffieHellmanPackage in diffieHellmanPackages)
                {
                    if(diffieHellmanPackage.uid == rsaPackage.ownerUID)
                    {
                        diffieHellmanKey = diffieHellmanPackage.GetFinalKey();
                        break;
                    }
                }

                //4. Encrypt everything and save it to the encrypted package
                EncryptedPackage encryptedPackage = CryptoClass.Encrypt(message, diffieHellmanKey, rsa);

                //5. Send package to the target user
                messagePacket.WriteUID(rsaPackage.ownerUID);
                messagePacket.WriteEncryptedPackage(encryptedPackage);
                _client.Client.Send(messagePacket.GetPacketBytes());
            }
        }
    }
}
