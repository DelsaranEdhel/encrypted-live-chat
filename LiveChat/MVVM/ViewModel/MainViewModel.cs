﻿using ChatClient.MVVM.Core;
using ChatClient.MVVM.Model;
using ChatClient.NET;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace ChatClient.MVVM.ViewModel
{
    class MainViewModel
    {
        public ObservableCollection<UserModel> Users { get; set; }
        public ObservableCollection<string> Messages { get; set; }
        public RelayCommand ConnectToServerCommand { get; set; } //assigned to "Connect" button
        public RelayCommand SendMessageCommand { get; set; } //assigned to "Send" button
        public string Username { get; set; }
        public string Message { get; set; }
        private Server server;

        public MainViewModel()
        {
            Users = new ObservableCollection<UserModel>();
            Messages = new ObservableCollection<string>();
            server = new Server();
            server.connectedEvent += UserConnected;
            server.msgReceivedEventTest += MessageReceived;
            server.userDisconnectEvent += RemoveUser;
            //first argument - what to do when "Connect" button will be clicked; second argument - enable or disable button interactivity
            ConnectToServerCommand = new RelayCommand(o => server.ConnectToServer(Username), o => !string.IsNullOrEmpty(Username));
            SendMessageCommand = new RelayCommand(o => SendMessage(), o => !string.IsNullOrEmpty(Message));
        }

        private void SendMessage()
        {
            server.SendMessageToServer(Message);
            MessageReceived($"[{DateTime.Now}] {Username}: " + Message);
        }

        private void UserConnected()
        {
            var user = new UserModel(server.packetReader.ReadMessage(), server.packetReader.ReadMessage());

            if(!Users.Any(x => x.UID == user.UID))
                Application.Current.Dispatcher.Invoke(() => Users.Add(user));

            MessageReceived("[" + user.Username + "] has connected!");
        }

        private void MessageReceived(string message)
        {
            Application.Current.Dispatcher.Invoke(() => Messages.Add(message));
        }

        private void RemoveUser()
        {
            Guid uid = server.packetReader.ReadUID();
            string message = server.packetReader.ReadMessage();

            var user = Users.Where(x => x.UID == uid.ToString()).FirstOrDefault();
            Application.Current.Dispatcher.Invoke(() => Users.Remove(user));

            MessageReceived(message);
        }
    }
}
