﻿using System.Windows;
using System.Windows.Controls;

namespace LiveChat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnSendClick(object sender, RoutedEventArgs e)
        {
            //When button has OnClick, then the Command will not execute, so we need do it anyway, but manually
            Button thisButton = sender as Button;
            thisButton.Command.Execute(thisButton.CommandParameter);

            messageTextBox.Text = string.Empty;
        }
    }
}
