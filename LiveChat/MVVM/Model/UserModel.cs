﻿namespace ChatClient.MVVM.Model
{
    /// <summary>
    /// Simple model which stores a Username and a UID
    /// A Username is being used for displaying a username on the users list (graphically)
    /// </summary>

    class UserModel
    {
        public string Username { get; set; }
        public string UID { get; set; }

        public UserModel(string Username, string UID)
        {
            this.Username = Username;
            this.UID = UID;
        }
    }
}
