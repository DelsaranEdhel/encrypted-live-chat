﻿using ChatClient.Net.IO;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ChatClient.Crypto
{
    class CryptoClass
    {
        //128, 192, or 256 bits (16, 24, or 32 bytes)

        public static EncryptedPackage Encrypt(string message, byte[] diffieHellmanKey, RSACryptoServiceProvider _rsa)
        {
            //1. Create AES
            Aes aes = Aes.Create();
            //1.1. Aes key is a Diffie-Hellman key material (our secret key)
            aes.Key = diffieHellmanKey;
            aes.GenerateIV();

            //2. Encrypt the message using AES
            byte[] encryptedMessage = EncryptAES(message, aes.Key, aes.IV);

            //3. Save RSA public key
            RSAParameters publicKey = _rsa.ExportParameters(false);

            //4. Encrypt the message and AES IV with RSA
            byte[] encryptedAESIV = EncryptRSA(aes.IV, publicKey);
            byte[] encryptedMessageWithRSA = EncryptRSA(encryptedMessage, publicKey);

            //5. Save everything into the package
            EncryptedPackage encryptedPackage = new EncryptedPackage(
                Convert.ToBase64String(encryptedMessageWithRSA),
                Convert.ToBase64String(encryptedAESIV)
                );

            return encryptedPackage;
        }

        public static string Decrypt(EncryptedPackage encryptedPackage, byte[] diffieHellmanKey, RSACryptoServiceProvider _rsa)
        {
            //1. Save RSA private key
            RSAParameters privateKey = _rsa.ExportParameters(true);

            //2. Decrypt the message and AES IV with RSA
            byte[] decryptedAESIV = DecryptRSA(Convert.FromBase64String(encryptedPackage.encryptedAESIV), privateKey);
            byte[] decryptedMessage = DecryptRSA(Convert.FromBase64String(encryptedPackage.encryptedMessage), privateKey);

            //3. Decrypt the message using AES
            return DecryptAES(decryptedMessage, diffieHellmanKey, decryptedAESIV);
        }

        static byte[] EncryptAES(string message, byte[] key, byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        byte[] plaintextBytes = Encoding.UTF8.GetBytes(message);
                        cryptoStream.Write(plaintextBytes, 0, plaintextBytes.Length);
                        cryptoStream.FlushFinalBlock();
                        return memoryStream.ToArray();
                    }
                }
            }
        }

        static string DecryptAES(byte[] encryptedMessage, byte[] key, byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(encryptedMessage, 0, encryptedMessage.Length);
                        cryptoStream.FlushFinalBlock();
                        byte[] decryptedBytes = memoryStream.ToArray();
                        return Encoding.UTF8.GetString(decryptedBytes, 0, decryptedBytes.Length);
                    }
                }
            }
        }

        static byte[] EncryptRSA(byte[] data, RSAParameters publicKey)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(publicKey);
                return rsa.Encrypt(data, false);
            }
        }

        static byte[] DecryptRSA(byte[] encryptedData, RSAParameters privateKey)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(privateKey);
                return rsa.Decrypt(encryptedData, false);
            }
        }
    }
}
