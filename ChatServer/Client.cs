﻿using ChatServer.Net.IO;
using System.Net.Sockets;

namespace ChatServer
{
    /// <summary>
    /// Client on the server side
    /// </summary>

    class Client
    {
        public string Username { get; set; }
        public Guid UID { get; set; }
        public TcpClient ClientSocket { get; set; }
        private PacketReader packetReader;

        public Client(TcpClient client)
        {
            ClientSocket = client;
            UID = Guid.NewGuid();
            packetReader = new PacketReader(ClientSocket.GetStream());

            var opcode = packetReader.ReadByte();
            Username = packetReader.ReadMessage();

            Console.WriteLine($"[{DateTime.Now}]: Client has connected with username: {Username}");

            //After a client was created then this process will be always executing (it need to listen for any packets)
            Task.Run(() => Process());
        }

        void Process()
        {
            while(true)
            {
                try
                {
                    var opcode = packetReader.ReadByte();
                    /*
                     * Opcodes:
                     * 3 - broadcast RSA public key
                     * 4 - broadcast Diffie-Hellman public key and request the key of the other user
                     * 5 - broadcast message
                     * 6 - broadcast Diffie-Hellman public key without requesting, because keys were exchanged
                     */
                    switch(opcode)
                    {
                        case 3:
                            string publicRSAKey = packetReader.ReadPublicRSAKey();
                            Console.WriteLine($"[{DateTime.Now}]: Broadcasting public RSA key = {publicRSAKey}");
                            Program.BroadcastRSAPublicKey(UID, publicRSAKey);
                            break;
                        case 4:
                            Guid uidFirst = packetReader.ReadUID();
                            byte[] publicDiffieHellmanKeyFirst = packetReader.ReadDiffieHellmanKey();
                            Program.BroadcastDiffieHellman(uidFirst, UID, publicDiffieHellmanKeyFirst);
                            break;
                        case 5:
                            Guid targetUid = packetReader.ReadUID();
                            EncryptedPackage msg = packetReader.ReadEncryptedPackage();
                            Console.WriteLine($"[{DateTime.Now}]: Broadcasting encrypted message = {msg.encryptedMessage}");
                            Program.BroadcastMessage(targetUid, UID, Username, msg);
                            break;
                        case 6:
                            var uidSecond = packetReader.ReadUID();
                            byte[] publicDiffieHellmanKeySecond = packetReader.ReadDiffieHellmanKey();
                            Program.BroadcastDiffieHellmanFinal(uidSecond, UID, publicDiffieHellmanKeySecond);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine($"[{UID.ToString()}]: Disconnected!");
                    Program.BroadcastDisconnect(UID.ToString());
                    ClientSocket.Close();
                    break;
                }
            }
        }
    }
}
