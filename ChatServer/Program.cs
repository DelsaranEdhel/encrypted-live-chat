﻿using ChatServer.Net.IO;
using System.Net;
using System.Net.Sockets;

namespace ChatServer
{
    /// <summary>
    /// Main server which connects every client
    /// </summary>
    class Program
    {
        //stores all users that are connected to the server
        private static List<Client> users;
        private static TcpListener listener;

        /// <summary>
        /// Stores a publicRSAKey and the GUID of the user to which this key belong
        /// </summary>
        public struct RsaPackage
        {
            public Guid ownerUID;
            public string publicRSAKey;

            public RsaPackage(Guid uid, string key)
            {
                this.ownerUID = uid;
                this.publicRSAKey = key;
            }
        }

        public static List<RsaPackage> rsaPackages = new List<RsaPackage>();

        static void Main(string[] args)
        {
            users = new List<Client>();
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 7891);
            listener.Start();

            while(true)
            {
                var client = new Client(listener.AcceptTcpClient());
                users.Add(client);

                BroadcastConnection();
            }
        }

        /// <summary>
        /// Inform other users that a new user has joined
        /// </summary>
        static void BroadcastConnection()
        {
            foreach(var user in users)
            {
                foreach(var usr in users)
                {
                    if (usr.UID == user.UID)
                        continue;

                    var broadcastPacket = new PacketBuilder();
                    broadcastPacket.WriteOpCode(1);
                    broadcastPacket.WriteMessage(usr.Username);
                    broadcastPacket.WriteMessage(usr.UID.ToString());
                    user.ClientSocket.Client.Send(broadcastPacket.GetPacketBytes());
                }
            }
        }

        /// <summary>
        /// Send the public RSA key and its user UID to every other user on the server
        /// </summary>
        /// <param name="uid">Key owner GUID</param>
        /// <param name="key">Public RSA key</param>
        public static void BroadcastRSAPublicKey(Guid uid, string key)
        {
            rsaPackages.Add(new RsaPackage(uid, key));

            foreach(var package in rsaPackages)
            {
                foreach (var user in users)
                {
                    if (user.UID.Equals(package.ownerUID))
                        continue;

                    var msgPacket = new PacketBuilder();
                    msgPacket.WriteOpCode(3);
                    msgPacket.WriteMessage(package.ownerUID.ToString());
                    msgPacket.WritePublicRSAKey(package.publicRSAKey);
                    user.ClientSocket.Client.Send(msgPacket.GetPacketBytes());
                }
            }
        }

        /// <summary>
        /// Send the public Diffie-Hellman key to every other user on the server
        /// Send with opcode = 4, which means this user (fromUid) request a public diffie-hellman key from targetUid user
        /// e.g. User A started sending a diffie-hellman key - he uses this method
        /// </summary>
        /// <param name="targetUid">Target user to which the key will be send</param>
        /// <param name="fromUid">From which user the key was sent</param>
        /// <param name="msg">Public Diffie-Hellman key</param>
        public static void BroadcastDiffieHellman(Guid targetUid, Guid fromUid, byte[] msg)
        {
            foreach(var user in users)
            {
                if (user.UID != targetUid)
                    continue;

                var msgPacket = new PacketBuilder();
                msgPacket.WriteOpCode(4);
                msgPacket.WriteUID(fromUid);
                msgPacket.WriteDiffieHellmanKey(msg);
                user.ClientSocket.Client.Send(msgPacket.GetPacketBytes());
                break;
            }
        }

        /// <summary>
        /// Send the public Diffie-Hellman key to every other user on the server
        /// Send with opcode = 6, which means that user fromUid is sending back his diffie-hellman key to the targetUid user (after this, keys were exchanged)
        /// e.g. User A was first to sent a diffie-hellman key, so user B is sending him his diffie-hellman key - he (B) uses this method
        /// </summary>
        /// <param name="targetUid">Target user to which the key will be send</param>
        /// <param name="fromUid">From which user the key was sent</param>
        /// <param name="msg">Public Diffie-Hellman key</param>
        public static void BroadcastDiffieHellmanFinal(Guid targetUid, Guid fromUid, byte[] msg)
        {
            foreach (var user in users)
            {
                if (user.UID != targetUid)
                    continue;

                var msgPacket = new PacketBuilder();
                msgPacket.WriteOpCode(6);
                msgPacket.WriteUID(fromUid);
                msgPacket.WriteDiffieHellmanKey(msg);
                user.ClientSocket.Client.Send(msgPacket.GetPacketBytes());
                break;
            }
        }

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="uid">Target user to which send the message</param>
        /// <param name="sourceUID">User that sent the message</param>
        /// <param name="Username">Useername that sent the message</param>
        /// <param name="package">EncryptedPackage with encrypted data</param>
        public static void BroadcastMessage(Guid uid, Guid sourceUID, string Username, EncryptedPackage package)
        {
            foreach (var user in users)
            {
                if (user.UID != uid)
                    continue;

                var msgPacket = new PacketBuilder();
                msgPacket.WriteOpCode(5);
                msgPacket.WriteUID(sourceUID);
                msgPacket.WriteMessage(Username);
                msgPacket.WriteEncryptedPackage(package);
                user.ClientSocket.Client.Send(msgPacket.GetPacketBytes());
            }
        }

        /// <summary>
        /// Inform other users that a user has disconnected from the server
        /// </summary>
        /// <param name="uid">User uid who disconnected from the server</param>
        public static void BroadcastDisconnect(string uid)
        {
            var disconnectedUser = users.Where(x => x.UID.ToString() == uid).FirstOrDefault();
            users.Remove(disconnectedUser);

            foreach (var user in users)
            {
                var broadcastPacket = new PacketBuilder();
                broadcastPacket.WriteOpCode(10);
                broadcastPacket.WriteUID(Guid.Parse(uid));
                broadcastPacket.WriteMessage($"[{disconnectedUser.Username}] Disconnected!");
                user.ClientSocket.Client.Send(broadcastPacket.GetPacketBytes());
            }
        }
    }
}