﻿using System.Text;

namespace ChatServer.Net.IO
{
    /// <summary>
    /// To write a message to a MemoryStream we need to:
    /// 1. Get the length of our message - if it is string, then its length, otherwise we are converting our data to a byte array and use a length of this array
    /// 2. We are writing a length in bytes
    /// 3. We are writing a message in bytes
    /// 
    /// The order of writing is also important, for example:
    /// We saved UID -> Message -> DiffieHellmanKey
    /// So when we will be reading, we need do it in the same order UID -> Message -> DiffieHellmanKey
    /// </summary>

    class PacketBuilder
    {
        MemoryStream ms;

        public PacketBuilder()
        {
            ms = new MemoryStream();
        }

        public void WriteOpCode(byte opcode)
        {
            ms.WriteByte(opcode);
        }

        public void WriteMessage(string msg)
        {
            var msgLength = msg.Length;
            ms.Write(BitConverter.GetBytes(msgLength));
            ms.Write(Encoding.ASCII.GetBytes(msg));
        }

        public void WriteDiffieHellmanKey(byte[] msg)
        {
            var msgLength = msg.Length;
            ms.Write(BitConverter.GetBytes(msgLength));
            ms.Write(msg);
        }

        public void WriteUID(Guid uid)
        {
            string msg = uid.ToString();
            var msgLength = msg.Length;
            ms.Write(BitConverter.GetBytes(msgLength));
            ms.Write(Encoding.ASCII.GetBytes(msg));
        }

        public void WriteEncryptedPackage(EncryptedPackage encryptedPackage)
        {
            byte[] myPackage = EncryptedPackage.GetBytes(encryptedPackage);
            var msgLength = myPackage.Length;

            ms.Write(BitConverter.GetBytes(msgLength));
            ms.Write(myPackage);
        }

        public void WritePublicRSAKey(string key)
        {
            var keyLength = key.Length;
            ms.Write(BitConverter.GetBytes(keyLength));
            ms.Write(Encoding.ASCII.GetBytes(key));
        }

        public byte[] GetPacketBytes()
        {
            return ms.ToArray();
        }
    }
}
