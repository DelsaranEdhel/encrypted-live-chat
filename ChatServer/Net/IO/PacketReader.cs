﻿using System.Net.Sockets;
using System.Text;

namespace ChatServer.Net.IO
{
    /// <summary>
    /// To read a message from a NetworkStream we need to:
    /// 1. Get the length of Int32 - ReadInt32()
    /// 2. Create a byte[] and set it to new byte[length]
    /// 3. We are reading from NetworkStream by giving a byte[] in which the message will be saved, offset 0 to read everything and length (count)
    /// 4. Based on what was saved we can convert our byte[] to our type (e.g. string)
    /// 
    /// We must read the data in order in which it was saved, for example:
    /// We saved UID -> Message -> DiffieHellmanKey
    /// So we must read it in the same order UID -> Message -> DiffieHellmanKey
    /// </summary>

    class PacketReader : BinaryReader
    {
        private NetworkStream ns;

        public PacketReader(NetworkStream ns) : base(ns)
        {
            this.ns = ns;
        }

        public EncryptedPackage ReadEncryptedPackage()
        {
            byte[] msgBuffer;
            var length = ReadInt32();
            msgBuffer = new byte[length];
            ns.Read(msgBuffer, 0, length);

            EncryptedPackage myPackage = EncryptedPackage.FromBytes(msgBuffer);
            return myPackage;
        }

        public string ReadMessage()
        {
            byte[] msgBuffer;
            var length = ReadInt32();
            msgBuffer = new byte[length];
            ns.Read(msgBuffer, 0, length);

            var msg = Encoding.ASCII.GetString(msgBuffer);
            return msg;
        }

        public byte[] ReadDiffieHellmanKey()
        {
            byte[] msgBuffer;
            var length = ReadInt32();
            msgBuffer = new byte[length];
            ns.Read(msgBuffer, 0, length);

            return msgBuffer;
        }

        public Guid ReadUID()
        {
            byte[] msgBuffer;
            var length = ReadInt32();
            msgBuffer = new byte[length];
            ns.Read(msgBuffer, 0, length);

            var msg = Encoding.ASCII.GetString(msgBuffer);
            return Guid.Parse(msg);
        }

        public string ReadPublicRSAKey()
        {
            byte[] msgBuffer;
            var length = ReadInt32();
            msgBuffer = new byte[length];
            ns.Read(msgBuffer, 0, length);

            var msg = Encoding.ASCII.GetString(msgBuffer);
            return msg;
        }
    }
}
